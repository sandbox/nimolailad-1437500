<div class="tri-Post">
    <div class="tri-Post-body">
<div class="tri-Post-inner">

	<div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) echo ' comment-unpublished'; ?>">
<h2 class="tri-PostHeader"> 
			<?php if ($title) {echo $title; } ?>

		</h2>
		
		<?php if ($submitted): ?>
			<span class="submitted"><?php echo $submitted; ?></span>
			<div class="cleared"></div>
		<?php endif; ?>	
		<?php if ($comment->new) : ?>
			<span class="new"><?php print drupal_ucfirst($new) ?></span>
		<?php endif; ?>
<div class="tri-PostContent">
		
			<div class="tri-triicle">
				<?php print $picture ?>
				<?php echo $content; ?>
			</div>

		</div>
		<div class="cleared"></div>
		
		<div class="links"><?php echo $links; ?><div class="cleared"></div></div>	
	</div>

</div>

    </div>
</div>
