<div class="tri-Block clear-block block block-<?php print $block->module ?>" id="block-<?php print $block->module .'-'. $block->delta; ?>">
    <div class="tri-Block-tl"></div>
    <div class="tri-Block-tr"></div>
    <div class="tri-Block-bl"></div>
    <div class="tri-Block-br"></div>
    <div class="tri-Block-tc"></div>
    <div class="tri-Block-bc"></div>
    <div class="tri-Block-cl"></div>
    <div class="tri-Block-cr"></div>
    <div class="tri-Block-cc"></div>
    <div class="tri-Block-body">

	<?php if ($block->subject): ?>
<div class="tri-BlockHeader">
		    <div class="l"></div>
		    <div class="r"></div>
		    <div class="tri-header-tag-icon">
		        <div class="t">	
			<h2 class="subject"><?php echo $block->subject; ?></h2>
</div>
		    </div>
		</div>    
	<?php endif; ?>
<div class="tri-BlockContent content">
	    <div class="tri-BlockContent-body">
	
		<?php echo $block->content; ?>

	    </div>
	</div>
	

    </div>
</div>
