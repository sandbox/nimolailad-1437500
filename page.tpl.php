<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo get_page_language($language); ?>" xml:lang="<?php echo get_page_language($language); ?>">

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />  
  <?php echo $head; ?>
  <title><?php if (isset($head_title )) { echo $head_title; } ?></title>  
  <?php echo $styles ?>
  <?php echo $scripts ?>
  <!--[if IE 6]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie6.css" type="text/css" /><![endif]-->  
  <!--[if IE 7]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie7.css" type="text/css" media="screen" /><![endif]-->
  <script type="text/javascript">if (Drupal.jsEnabled) {$(document).ready(function(){
				window.setTimeout("triNoStyleAdding(document)", 2000);});}</script>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body oncontextmenu="return false" onselectsttri="return false" ondragsttri="return false">
    <div id="tri-page-background-gradient"></div>

 <div class="tri-Logo">
     <?php   if (!empty($site_name)) { echo '<h1 class="tri-Logo-name"><a href="'.check_url($base_path).'" title = "'.$site_name.'"><img src="/sites/all/themes/WaterDrop/images/logo.jpg" align="middle" />&nbsp;'.$site_name.'</a></h1>'; } ?>
     <?php   if (!empty($site_slogan)) { echo '<div class="tri-Logo-text">'.$site_slogan.'</div>'; } ?>
</div>

<div id="tri-main">

  
<div class="tri-Sheet">
    <div class="tri-Sheet-tl"></div>
    <div class="tri-Sheet-tr"></div>
    <div class="tri-Sheet-bl"></div>
    <div class="tri-Sheet-br"></div>
    <div class="tri-Sheet-tc"></div>
    <div class="tri-Sheet-bc"></div>
    <div class="tri-Sheet-cl"></div>
    <div class="tri-Sheet-cr"></div>
    <div class="tri-Sheet-cc"></div>
    <div class="tri-Sheet-body">
    
    
<?php if (!empty($navigation)): ?>
<div class="tri-nav">
        <div class="l"></div>
    <div class="r"></div>
            <?php echo $navigation; ?>
	</div>
<?php endif;?>
<div class="tri-Header">
    <div class="tri-Header-jpeg"></div>

</div>
<?php if (!empty($banner1)) { echo '<div id="banner1">'.$banner1.'</div>'; } ?>
<?php echo tri_placeholders_output($top1, $top2, $top3); ?>
<div class="tri-contentLayout">
<div class="<?php echo (!empty($right) || !empty($sidebar_right)) ? 'tri-content' : 'tri-content-wide'; ?>">
<?php if (!empty($banner2)) { echo '<div id="banner2">'.$banner2.'</div>'; } ?>
<?php if ((!empty($user1)) && (!empty($user2))) : ?>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr valign="top"><td width="50%"><?php echo $user1; ?></td>
<td><?php echo $user2; ?></td></tr>
</table>
<?php else: ?>
<?php if (!empty($user1)) { echo '<div id="user1">'.$user1.'</div>'; }?>
<?php if (!empty($user2)) { echo '<div id="user2">'.$user2.'</div>'; }?>
<?php endif; ?>
<?php if (!empty($banner3)) { echo '<div id="banner3">'.$banner3.'</div>'; } ?>
<?php if (($is_front) || (isset($node) && isset($node->nid))): ?>              
<?php if (!empty($breadcrumb) || !empty($tabs) || !empty($tabs2)): ?>
<div class="tri-Post">
    <div class="tri-Post-body">
<div class="tri-Post-inner">
<div class="tri-PostContent">
<?php if (!empty($breadcrumb)) { echo $breadcrumb; } ?>
<?php if (!empty($tabs)) { echo $tabs.'<div class="cleared"></div>'; }; ?>
<?php if (!empty($tabs2)) { echo $tabs2.'<div class="cleared"></div>'; } ?>

</div>
<div class="cleared"></div>

</div>

    </div>
</div>
<?php endif; ?>
<?php if (!empty($mission)) { echo '<div id="mission">'.$mission.'</div>'; }; ?>
<?php if (!empty($help)) { echo $help; } ?>
<?php if (!empty($messages)) { echo $messages; } ?>
<?php echo tri_content_replace($content); ?>
<?php else: ?>
<div class="tri-Post">
    <div class="tri-Post-body">
<div class="tri-Post-inner">
<div class="tri-PostContent">
<?php if (!empty($breadcrumb)) { echo $breadcrumb; } ?>
<?php if (!empty($title)): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
<?php if (!empty($tabs)) { echo $tabs.'<div class="cleared"></div>'; }; ?>
<?php if (!empty($tabs2)) { echo $tabs2.'<div class="cleared"></div>'; } ?>
<?php if (!empty($mission)) { echo '<div id="mission">'.$mission.'</div>'; }; ?>
<?php if (!empty($help)) { echo $help; } ?>
<?php if (!empty($messages)) { echo $messages; } ?>
<?php echo tri_content_replace($content); ?>

</div>
<div class="cleared"></div>

</div>

    </div>
</div>
<?php endif; ?>
<?php if (!empty($banner4)) { echo '<div id="banner4">'.$banner4.'</div>'; } ?>
<?php if (!empty($user3) && !empty($user4)) : ?>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr valign="top"><td width="50%"><?php echo $user3; ?></td>
<td><?php echo $user4; ?></td></tr>
</table>
<?php else: ?>
<?php if (!empty($user3)) { echo '<div id="user1">'.$user3.'</div>'; }?>
<?php if (!empty($user4)) { echo '<div id="user2">'.$user4.'</div>'; }?>
<?php endif; ?>
<?php if (!empty($banner5)) { echo '<div id="banner5">'.$banner5.'</div>'; } ?>
</div>
<?php if (!empty($right)) echo '<div class="tri-sidebar1">' . $right . "</div>"; 
else if (!empty($sidebar_right)) echo '<div class="tri-sidebar1">' . $sidebar_right . "</div>";?>

</div>
<div class="cleared"></div>
<?php echo tri_placeholders_output($bottom1, $bottom2, $bottom3); ?>
<?php if (!empty($banner6)) { echo '<div id="banner6">'.$banner6.'</div>'; } ?>
<div class="tri-Footer">
    <div class="tri-Footer-inner">
        <?php echo tri_feed_icon(url('rss.xml')); ?>
        <div class="tri-Footer-text">
        <?php 
            if (!empty($footer_message) && (trim($footer_message) != '')) { 
                echo $footer_message;
            }
            else {
                echo '<p><a href="#">Contact Us</a>&nbsp;|&nbsp;<a href="#">Terms of Use</a>&nbsp;|&nbsp;<a href="#">Trademarks</a>&nbsp;|&nbsp;<a href="#">Privacy Statement</a><br />'.
                     'Copyright &copy; 2009&nbsp;'.$site_name.'.&nbsp;All Rights Reserved. Designed by TribecaSys</p>';
            }
        ?>
        <?php if (!empty($copyright)) { echo $copyright; } ?>
        </div>        
    </div>
    <div class="tri-Footer-background"></div>
</div>

    </div>
</div>
<div class="cleared"></div>
<p class="tri-page-footer"><a href="http://tribecasys.com" target="_blank">Designed by TribecaSys</a></p>
</div>


<?php print $closure; ?>

</body>
</html>