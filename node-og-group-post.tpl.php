<div class="tri-Post">
    <div class="tri-Post-body">
<div class="tri-Post-inner">
<h2 class="tri-PostHeader"> <?php echo tri_node_title_output($title, $node_url, $page); ?>
</h2>
<div class="tri-PostContent">
<div class="tri-triicle"><?php print $picture; ?><?php echo $content; ?>
<?php if (isset($node->links['node_read_more'])) { echo '<div class="read_more">'.get_html_link_output($node->links['node_read_more']).'</div>'; }?></div>
</div>
<div class="cleared"></div>

</div>

    </div>
</div>
